use crate::days::*;
use crate::secret;
use reqwest::{cookie::Jar, Url};
use std::collections::HashMap;
use std::fs;
use std::sync::Arc;
use std::time::Instant;

/// Parses the list of desired days to run.
///
/// # Arguments
/// * `days` - A `String` representing a list of days. Element of the list should be integers
/// between 1 and 25 (inclusive) or ranges of the form `n-m` with `n` and `m` integers between 1
/// and 25. Elements should be separated by commas (`,`). Spaces are tolerated.
///
/// # Returns
/// A sorted vector containing no duplicates.
///
/// # Panics
/// This will panic in the case of bad range formatting, that is having more than one hyphen per
/// range (eg. `1-3-6`).
pub fn parse_days(days: &str) -> Vec<usize> {
    let mut res = days
        .trim()
        .split(',') // Getting list elements
        .map(|range| {
            let bounds = range.trim().split('-').collect::<Vec<&str>>();
            match bounds.len() {
                0 => Vec::new(), // In case multiple commas are found, an empty element is produced
                1 => {
                    // Singleton
                    let n = bounds.first().unwrap().parse::<usize>().unwrap();
                    if n > 25 {
                        println!("Can't run days greater than 25 ({})", n);
                        Vec::new()
                    } else {
                        vec![n]
                    }
                }
                2 => {
                    // Range
                    let b1 = bounds.first().unwrap().parse::<usize>().unwrap();
                    let b2 = bounds.get(1).unwrap().parse::<usize>().unwrap();
                    let mut v = Vec::new();
                    for n in b1..=b2 {
                        if n > 25 {
                            println!("Can't run days greater than 25 ({})", n)
                        } else {
                            v.push(n)
                        }
                    }
                    v
                }
                _ => panic!("Can't parse range {}", range),
            }
        })
        .collect::<Vec<Vec<usize>>>()
        .concat();
    res.sort_unstable();
    res.dedup();
    res
}

/// Automatically get my input for the day.
///
/// # Panics
/// This will panic if `day` is larger than 25, or if the corresponding puzzle hasn't been released yet.
pub fn get_input(day: usize, verbose: bool) -> String {
    if verbose {
        println!("Getting input for day {} ...", day)
    }
    // First, we check the assets folder exists, and create it otherwise
    let assets_dir_path = std::path::Path::new("./assets");
    if !assets_dir_path.exists() {
        if verbose {
            println!("    Creating assets directory")
        }
        fs::create_dir(assets_dir_path).unwrap();
    }

    // Then we check if the desired input has already been downloaded
    let input_local_path_string = format!("./assets/input{}", day);
    let input_local_path = std::path::Path::new(&input_local_path_string);
    if input_local_path.exists() {
        // Then we open the file
        if verbose {
            println!("    [{}] From file", day)
        }
        fs::read_to_string(input_local_path).unwrap()
    } else {
        // Else we download it and return its content
        // Session ID is a unique token, inputs may vary
        // To get yours, follow https://github.com/wimglenn/advent-of-code-wim/issues/1
        let session_cookie = secret::session_cookie();
        let url = format!("https://adventofcode.com/2022/day/{}/input", day)
            .parse::<Url>()
            .unwrap();
        let jar = Jar::default();
        jar.add_cookie_str(&session_cookie, &url);
        let client = reqwest::blocking::ClientBuilder::new()
            .cookie_provider(Arc::new(jar))
            .user_agent(
                "https://gitlab.com/Val.zero/advent-of-code-2022 - valentin.zero@protonmail.com",
            )
            .build()
            .unwrap();
        let input_content = client.get(url).send().unwrap().text().unwrap();
        fs::write(input_local_path, input_content.as_bytes()).unwrap();
        if verbose {
            println!("    [{}] Downloaded input", day)
        }
        input_content
    }
}

/// Call the day's first or second puzzle solver.
///
/// # Panics
/// This will panic is `part` isn't 1 or 2.
pub fn solver(day: usize, part: usize) -> fn(&str) -> usize {
    let functions: HashMap<_, _> = [
        (11, (day11::solve1 as for<'r> fn(&'r str) -> usize, day11::solve2 as for<'r> fn(&'r str) -> usize)),
        (10, (day10::solve1 as for<'r> fn(&'r str) -> usize, day10::solve2 as for<'r> fn(&'r str) -> usize)),
        (09, (day09::solve1 as for<'r> fn(&'r str) -> usize, day09::solve2 as for<'r> fn(&'r str) -> usize)),
        (08, (day08::solve1 as for<'r> fn(&'r str) -> usize, day08::solve2 as for<'r> fn(&'r str) -> usize)),
        (07, (day07::solve1 as for<'r> fn(&'r str) -> usize, day07::solve2 as for<'r> fn(&'r str) -> usize)),
        (06, (day06::solve1 as for<'r> fn(&'r str) -> usize, day06::solve2 as for<'r> fn(&'r str) -> usize)),
        (05, (day05::solve1 as for<'r> fn(&'r str) -> usize, day05::solve2 as for<'r> fn(&'r str) -> usize)),
        (04, (day04::solve1 as for<'r> fn(&'r str) -> usize, day04::solve2 as for<'r> fn(&'r str) -> usize)),
        (03, (day03::solve1 as for<'r> fn(&'r str) -> usize, day03::solve2 as for<'r> fn(&'r str) -> usize)),
        (02, (day02::solve1 as for<'r> fn(&'r str) -> usize, day02::solve2 as for<'r> fn(&'r str) -> usize)),
        (01, (day01::solve1 as for<'r> fn(&'r str) -> usize, day01::solve2 as for<'r> fn(&'r str) -> usize)),
    ].into();
    let f = functions.get(&day).unwrap();
    match part {
        0 => f.0,
        1 => f.1,
        _ => panic!("Part must be 1 or 2"),
    }
}

/// Times the execution of a function with a given input.
///
/// # Returns
/// A pair consisting of the result of the function call and the measured duration of the execution.
pub fn timer(fun: fn(&str) -> usize, input: &str) -> (usize, u128) {
    let t = Instant::now();
    let res = fun(input);
    let dt = t.elapsed().as_nanos();
    (res, dt)
}

/// Formats a duration in nanoseconds.
pub fn time_formatting(duration: u128) -> String {
    match duration {
        n if n > 1_000_000_000 => format!("{:.3} s", ((duration as f64) / 1_000_000_000.)),
        n if n > 1_000_000 => format!("{:.3} ms", ((duration as f64) / 1_000_000.)),
        n if n > 1_000 => format!("{:.3} µs", ((duration as f64) / 1_000.)),
        n => format!("{} ns", n),
    }
}
