pub fn solve1(input: &str) -> usize {
    input
        .lines()
        .map(|line| char_value(find_common_halves(line).unwrap()))
        .sum()
}

pub fn solve2(input: &str) -> usize {
    input
        .lines()
        .collect::<Vec<_>>()
        .chunks(3)
        .map(|group| char_value(find_common_three(group).unwrap()))
        .sum()
}

/// Find the common character in a string's halves.
fn find_common_halves(line: &str) -> Result<char, String> {
    let (fst, snd) = line.split_at(line.len() / 2);
    for c in fst.chars() {
        if snd.chars().any(|x| x == c) {
            return Ok(c);
        }
    }
    Err("No common character found.".to_string())
}

/// Find the common character in a group of three strings.
fn find_common_three(group: &[&str]) -> Result<char, String> {
    for c in group[0].chars() {
        if group[1].chars().any(|x| x == c) && group[2].chars().any(|x| x == c) {
            return Ok(c);
        }
    }
    Err("No common character found.".to_string())
}

/// Return the value associated with a character.
fn char_value(c: char) -> usize {
    if c.is_lowercase() {
        c as usize - 'a' as usize + 1
    } else {
        c as usize - 'A' as usize + 27
    }
}

#[cfg(test)]
mod tests {
    use crate::days::day03::*;

    #[test]
    fn test_day03_solve1() {
        let test_data = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";
        assert_eq!(solve1(test_data), 157)
    }

    #[test]
    fn test_day03_solve2() {
        let test_data = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";
        assert_eq!(solve2(test_data), 70)
    }
}
