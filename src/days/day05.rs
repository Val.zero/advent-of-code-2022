use regex::Regex;
use std::collections::VecDeque;
use std::iter::FromIterator;

pub fn solve1(input: &str) -> usize {
    let mut parts = input.split("\n\n");
    let stacks_input = parts.next().unwrap();
    let instr_input = parts.next().unwrap();

    let mut stacks = init_stacks(stacks_input);

    let instr_re = Regex::new(r"move (?P<n>\d+) from (?P<src>\d+) to (?P<dst>\d+)").unwrap();
    for line in instr_input.lines() {
        let capt = instr_re.captures(line).unwrap();
        let n = capt["n"].parse::<usize>().unwrap();
        let src = capt["src"].parse::<usize>().unwrap() - 1;
        let dst = capt["dst"].parse::<usize>().unwrap() - 1;
        for _ in 0..n {
            let e = stacks[src].pop_back().unwrap();
            stacks[dst].push_back(e);
        }
    }

    stacks_top(stacks)
}

pub fn solve2(input: &str) -> usize {
    let mut parts = input.split("\n\n");
    let stacks_input = parts.next().unwrap();
    let instr_input = parts.next().unwrap();

    let mut stacks = init_stacks(stacks_input);

    let instr_re = Regex::new(r"move (?P<n>\d+) from (?P<src>\d+) to (?P<dst>\d+)").unwrap();
    for line in instr_input.lines() {
        let capt = instr_re.captures(line).unwrap();
        let n = capt["n"].parse::<usize>().unwrap();
        let src = capt["src"].parse::<usize>().unwrap() - 1;
        let dst = capt["dst"].parse::<usize>().unwrap() - 1;
        let src_stack = &mut stacks[src];
        src_stack.make_contiguous();
        let slice = src_stack.as_slices().0[src_stack.len() - n..src_stack.len()].to_vec();
        for _ in 0..n {
            src_stack.pop_back();
        }
        for e in slice {
            stacks[dst].push_back(e);
        }
    }

    stacks_top(stacks)
}

/// Parse the initial state of the crate stacks.
fn init_stacks(stacks_input: &str) -> Vec<VecDeque<usize>> {
    // We first need the number of stacks ...
    let stacks_number = stacks_input
        .lines()
        .last() // ... which is on the last line ...
        .unwrap()
        .split_whitespace()
        .last() // ... and the last number.
        .unwrap()
        .parse::<usize>()
        .unwrap();
    let mut stacks = vec![VecDeque::new(); stacks_number];
    let crate_re = Regex::new(r"\[([A-Z])]").unwrap();
    for line in stacks_input.lines() {
        // The crates are located in each group of 4 characters of the input lines
        for (index, chunk) in line
            .chars()
            .collect::<Vec<_>>()
            .chunks(4)
            .map(|chunk| String::from_iter(chunk.iter()))
            .enumerate()
        {
            if let Some(capture) = crate_re.captures(chunk.as_str()) {
                stacks[index].push_front(letter_to_usize(capture.get(1).unwrap().as_str()))
            }
        }
    }
    stacks
}

/// Give the integer corresponding to a capital letter.
fn letter_to_usize(letter: &str) -> usize {
    letter.chars().next().unwrap() as usize - 'A' as usize + 1
}

/// Give the integer corresponding to the set of letters at the top of the stacks.
fn stacks_top(stacks: Vec<VecDeque<usize>>) -> usize {
    let mut res = 0;
    let mut msg = Vec::new();
    for mut stack in stacks {
        let elem = stack.pop_back().unwrap_or(0);
        res = res * 100 + elem;
        msg.push(elem as u8 + b'A' - 1);
    }
    println!("{}", String::from_utf8(msg).unwrap());
    res
}

#[cfg(test)]
mod tests {
    use crate::days::day05::*;

    #[test]
    fn test_day05_solve1() {
        let test_data = "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";
        assert_eq!(solve1(test_data), 31326)
    }

    #[test]
    fn test_day05_solve2() {
        let test_data = "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";
        assert_eq!(solve2(test_data), 130304)
    }
}
