pub fn solve1(input: &str) -> usize {
    solve_common(input, 4)
}

pub fn solve2(input: &str) -> usize {
    solve_common(input, 14)
}

/// Compute the index of the last character of the first sequence of n different characters in a given input.
fn solve_common(input: &str, n: usize) -> usize {
    let chars = input.chars().collect::<Vec<_>>();
    let size = input.chars().count();
    for i in n..size {
        if no_repeated_char(&chars[i - n..i], n) {
            return i;
        }
    }
    panic!("Didn't find a sequence of 4 different characters.")
}

/// Check that no repeated character appears in the given sequence.
fn no_repeated_char(chars: &[char], n: usize) -> bool {
    for i in 0..n {
        for j in i + 1..n {
            if chars[i] == chars[j] {
                return false;
            }
        }
    }
    true
}

#[cfg(test)]
mod tests {
    use crate::days::day06::*;

    #[test]
    fn test_day06_solve1() {
        let test_data = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
        assert_eq!(solve1(test_data), 7)
    }

    #[test]
    fn test_day06_solve2() {
        let test_data = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
        assert_eq!(solve2(test_data), 19)
    }
}
