pub fn solve1(input: &str) -> usize {
    input
        .split("\n\n") // Get each elf's items
        .map(|items| {
            items
                .lines()
                .map(|item| item.parse::<usize>().unwrap())
                .sum() // Sum up the elf's items
        })
        .max() // Get the maximum sum
        .unwrap()
}

pub fn solve2(input: &str) -> usize {
    let mut sums = input
        .split("\n\n") // Get each elf's items
        .map(|items| {
            items
                .lines()
                .map(|item| item.parse::<usize>().unwrap())
                .sum() // Sum up the elf's items
        })
        .collect::<Vec<usize>>();
    sums.sort();
    sums[sums.len() - 3..sums.len()].to_vec().iter().sum() // Get the sum of the three max elves
}

#[cfg(test)]
mod tests {
    use crate::days::day01::*;

    #[test]
    fn test_day01_solve1() {
        let test_data = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
        assert_eq!(solve1(test_data), 24000)
    }

    #[test]
    fn test_day01_solve2() {
        let test_data = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
        assert_eq!(solve2(test_data), 45000)
    }
}
