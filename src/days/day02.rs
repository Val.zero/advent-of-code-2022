pub fn solve1(input: &str) -> usize {
    input.lines().map(round_score).sum()
}

pub fn solve2(input: &str) -> usize {
    input.lines().map(round_score2).sum()
}

/// Computes a player's score for a round.
///
/// # Panics
/// This will panic if the round line is badly formatted (A letter from A to C, a space, then a letter
/// from X to Z).
fn round_score(round: &str) -> usize {
    let mut shapes = round.split(' ');
    let opponent = shapes.next().unwrap();
    let player = shapes.next().unwrap();
    if shapes.next().is_some() {
        panic!("More than two shapes in a round.")
    }
    shape_score(player).unwrap() + outcome_score(opponent, player).unwrap()
}

/// Computes the part of a player's round score based on the shape played.
fn shape_score(shape: &str) -> Result<usize, String> {
    match shape {
        "X" => Ok(1),
        "Y" => Ok(2),
        "Z" => Ok(3),
        _ => Err("Expected a letter from X to Z for the player's shape.".to_string()),
    }
}

/// Round outcome.
enum Outcome {
    Loss,
    Draw,
    Win,
}

/// Computes the part of a player's round score based on the outcome.
fn outcome_score(opponent: &str, player: &str) -> Result<usize, String> {
    let outcome = match opponent {
        // Opponent plays Rock
        "A" => match player {
            "X" => Ok(Outcome::Draw),
            "Y" => Ok(Outcome::Win),
            "Z" => Ok(Outcome::Loss),
            _ => Err("Expected a letter from X to Z for the player's shape."),
        },
        // Opponent plays Paper
        "B" => match player {
            "X" => Ok(Outcome::Loss),
            "Y" => Ok(Outcome::Draw),
            "Z" => Ok(Outcome::Win),
            _ => Err("Expected a letter from X to Z for the player's shape."),
        },
        // Opponent plays Scissors
        "C" => match player {
            "X" => Ok(Outcome::Win),
            "Y" => Ok(Outcome::Loss),
            "Z" => Ok(Outcome::Draw),
            _ => Err("Expected a letter from X to Z for the player's shape."),
        },
        _ => Err("Expected a letter from A to C for the opponent's shape."),
    };
    match outcome {
        Ok(Outcome::Loss) => Ok(0),
        Ok(Outcome::Draw) => Ok(3),
        Ok(Outcome::Win) => Ok(6),
        Err(msg) => Err(msg.to_string()),
    }
}

/// Computes a player's score for a round.
///
/// # Panics
/// This will panic if the round line is badly formatted (A letter from A to C, a space, then a letter
/// from X to Z).
fn round_score2(round: &str) -> usize {
    let mut shapes = round.split(' ');
    let opponent = shapes.next().unwrap();
    let wanted_outcome = to_outcome(shapes.next().unwrap()).unwrap();
    if shapes.next().is_some() {
        panic!("More than two shapes in a round.")
    }
    match opponent {
        // Opponent plays Rock
        "A" => match wanted_outcome {
            Outcome::Loss => 3 + 0,
            Outcome::Draw => 1 + 3,
            Outcome::Win => 2 + 6,
        },
        // Opponent plays Paper
        "B" => match wanted_outcome {
            Outcome::Loss => 1 + 0,
            Outcome::Draw => 2 + 3,
            Outcome::Win => 3 + 6,
        },
        // Opponent plays Scissors
        "C" => match wanted_outcome {
            Outcome::Loss => 2 + 0,
            Outcome::Draw => 3 + 3,
            Outcome::Win => 1 + 6,
        },
        _ => panic!("Expected a letter from A to C for the opponent's shape."),
    }
}

fn to_outcome(wanted: &str) -> Result<Outcome, String> {
    match wanted {
        "X" => Ok(Outcome::Loss),
        "Y" => Ok(Outcome::Draw),
        "Z" => Ok(Outcome::Win),
        _ => Err("Expected a letter from X to Z for the expected outcome.".to_string()),
    }
}

#[cfg(test)]
mod tests {
    use crate::days::day02::*;

    #[test]
    fn test_day02_solve1() {
        let test_data = "A Y
B X
C Z";
        assert_eq!(solve1(test_data), 15)
    }

    #[test]
    fn test_day02_solve2() {
        let test_data = "A Y
B X
C Z";
        assert_eq!(solve2(test_data), 12)
    }
}
