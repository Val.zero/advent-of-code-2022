pub fn solve1(input: &str) -> usize {
    let mut cycle = 0;
    let mut c_index = 0;
    let mut x = 1;
    let mut strengths = Vec::new();
    let mut lines = input.lines();
    let mut read = true;
    let mut buf = 0;

    while cycle <= 220 {
        cycle += 1;
        if cycle == 20 + 40 * c_index {
            strengths.push(cycle * x);
            c_index += 1;
        }

        if read {
            let line = lines.next().unwrap();
            if line != "noop" {
                let (_, n) = line.split_at(5);
                buf = n.parse::<isize>().unwrap();
                read = false;
            }
        } else {
            x += buf;
            read = true;
        }
    }

    strengths.iter().sum::<isize>() as usize
}

pub fn solve2(input: &str) -> usize {
    let mut cycle = 0;
    let mut x = 1;
    let mut pixels = vec![false; 240];
    let mut lines = input.lines();
    let mut read = true;
    let mut buf = 0;

    while cycle <= 240 {
        cycle += 1;
        if (cycle % 40) as isize >= x && (cycle % 40) as isize <= x + 2 {
            pixels[cycle - 1] = true;
        }

        if read {
            if let Some(line) = lines.next() {
                if line != "noop" {
                    let (_, n) = line.split_at(5);
                    buf = n.parse::<isize>().unwrap();
                    read = false;
                }
            }
        } else {
            x += buf;
            read = true;
        }
    }

    print(pixels);
    0
}

/// Print a Boolean vector by lines of 40 characters.
fn print(pixels: Vec<bool>) {
    for i in 1..=240 {
        if pixels[i - 1] {
            print!("#")
        } else {
            print!(".")
        }
        if i % 40 == 0 {
            print!("\n")
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::days::day10::*;

    #[test]
    fn test_day10_solve1() {
        let test_data = "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";
        assert_eq!(solve1(test_data), 13140)
    }

    #[test]
    fn test_day10_solve2() {
        let test_data = "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";
        assert_eq!(solve2(test_data), 0)
    }
}
