pub fn solve1(input: &str) -> usize {
    // Parse input matrix
    let in_mat = input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| c.to_string().parse::<isize>().unwrap())
                .collect()
        })
        .collect::<Vec<Vec<_>>>();
    let x_len = in_mat.first().unwrap().len();
    let y_len = in_mat.len();

    // Make an equal-sized Boolean matrix
    let mut res_mat = vec![vec![false; x_len]; y_len];

    // Go through the input matrix in each direction
    let mut max;
    for x in 0..x_len {
        max = -1;
        for y in 0..y_len {
            if in_mat[y][x] > max {
                res_mat[y][x] = true;
                max = in_mat[y][x]
            }
        }
    }
    for x in 0..x_len {
        max = -1;
        for y in (0..y_len).rev() {
            if in_mat[y][x] > max {
                res_mat[y][x] = true;
                max = in_mat[y][x]
            }
        }
    }
    for y in 0..y_len {
        max = -1;
        for x in 0..x_len {
            if in_mat[y][x] > max {
                res_mat[y][x] = true;
                max = in_mat[y][x]
            }
        }
    }
    for y in 0..y_len {
        max = -1;
        for x in (0..x_len).rev() {
            if in_mat[y][x] > max {
                res_mat[y][x] = true;
                max = in_mat[y][x]
            }
        }
    }

    // Count the number of true
    res_mat
        .iter()
        .map(|row| row.iter().filter(|b| **b).count())
        .sum()
}

pub fn solve2(input: &str) -> usize {
    // Parse input matrix
    let in_mat = input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| c.to_string().parse::<isize>().unwrap())
                .collect()
        })
        .collect::<Vec<Vec<_>>>();
    let x_len = in_mat.first().unwrap().len();
    let y_len = in_mat.len();

    // Make an equal-sized integer matrix
    let mut res_mat = vec![vec![1; x_len]; y_len];

    // For each cell, go in each direction and count visible trees
    for x in 0..x_len {
        for y in 0..y_len {
            let heigth = in_mat[y][x];
            let mut up = 0;
            let mut down = 0;
            let mut left = 0;
            let mut right = 0;

            for up_y in (0..y).rev() {
                up += 1;
                if in_mat[up_y][x] >= heigth {
                    break;
                }
            }
            for down_y in y + 1..y_len {
                down += 1;
                if in_mat[down_y][x] >= heigth {
                    break;
                }
            }
            for left_x in (0..x).rev() {
                left += 1;
                if in_mat[y][left_x] >= heigth {
                    break;
                }
            }
            for right_x in x + 1..x_len {
                right += 1;
                if in_mat[y][right_x] >= heigth {
                    break;
                }
            }

            res_mat[y][x] = up * down * left * right
        }
    }

    // Find the max in the resulting matrix
    *res_mat
        .iter()
        .map(|row| row.iter().max().unwrap())
        .max()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use crate::days::day08::*;

    #[test]
    fn test_day08_solve1() {
        let test_data = "30373
25512
65332
33549
35390";
        assert_eq!(solve1(test_data), 21)
    }

    #[test]
    fn test_day08_solve2() {
        let test_data = "30373
25512
65332
33549
35390";
        assert_eq!(solve2(test_data), 8)
    }
}
