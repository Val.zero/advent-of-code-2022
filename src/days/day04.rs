pub fn solve1(input: &str) -> usize {
    input
        .lines()
        .filter(|line| ranges_pair_predicate(parse_pairs(line), contains))
        .count()
}

pub fn solve2(input: &str) -> usize {
    input
        .lines()
        .filter(|line| ranges_pair_predicate(parse_pairs(line), overlap))
        .count()
}

/// Positive integers range.
type Range = (usize, usize);

/// Make a pair of ranges from an input line.
fn parse_pairs(line: &str) -> (Range, Range) {
    let numbers = line
        .split(',') // Ranges are separated by ","
        .map(|range| {
            range
                .split('-') // Bounds are separated by "-"
                .map(|num| num.parse::<usize>().unwrap())
                .collect()
        })
        .collect::<Vec<Vec<_>>>();
    (
        (numbers[0][0], numbers[0][1]),
        (numbers[1][0], numbers[1][1]),
    )
}

/// Check if a pair of ranges validates a predicate in any order.
fn ranges_pair_predicate(ranges: (Range, Range), pred: fn(Range, Range) -> bool) -> bool {
    let (r1, r2) = ranges;
    pred(r1, r2) || pred(r2, r1)
}

/// Check if one range fully contains another.
fn contains(range: Range, other: Range) -> bool {
    let (x1, y1) = range;
    let (x2, y2) = other;
    (x1 <= x2) && (y1 >= y2)
}

/// Check if one range overlaps on its left with another.
fn overlap(range: Range, other: Range) -> bool {
    let (x1, _) = range;
    let (x2, y2) = other;
    (x1 >= x2) && (x1 <= y2)
}

#[cfg(test)]
mod tests {
    use crate::days::day04::*;

    #[test]
    fn test_day04_solve1() {
        let test_data = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";
        assert_eq!(solve1(test_data), 2)
    }

    #[test]
    fn test_day04_solve2() {
        let test_data = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";
        assert_eq!(solve2(test_data), 4)
    }
}
