use std::collections::HashMap;

pub fn solve1(input: &str) -> usize {
    let mut res_set = HashMap::new();
    let mut hd = (0, 0);
    let mut tl = (0, 0);
    res_set.insert(tl, true);

    for line in input.lines() {
        let (dir, n) = parse_instr(line);
        for _ in 0..n {
            hd = apply_move(&hd, &dir);
            tl = follow(&tl, &hd);
            res_set.insert(tl, true);
        }
    }

    res_set.keys().count()
}

pub fn solve2(input: &str) -> usize {
    let mut res_set = HashMap::new();
    let mut rope = vec![(0, 0); 10];
    res_set.insert(*rope.last().unwrap(), true);

    for line in input.lines() {
        let (dir, n) = parse_instr(line);
        for _ in 0..n {
            let mut new_rope = Vec::new();
            new_rope.push(apply_move(rope.first().unwrap(), &dir));
            for i in 1..rope.len() {
                new_rope.push(follow(&rope[i], &new_rope[i - 1]))
            }
            rope = new_rope;
            res_set.insert(*rope.last().unwrap(), true);
        }
    }

    res_set.keys().count()
}

/// A two-dimensional point.
type Point = (isize, isize);

/// A two-dimensional direction.
enum Dir {
    Up,
    Down,
    Left,
    Right,
}

impl Dir {
    /// Make a new direction from a single-character string.
    fn new(s: &str) -> Self {
        match s {
            "U" => Dir::Up,
            "D" => Dir::Down,
            "L" => Dir::Left,
            "R" => Dir::Right,
            _ => panic!("Unknown direction {}", s),
        }
    }
}

/// Parse an instruction in the form "<direction character> <number>".
fn parse_instr(line: &str) -> (Dir, usize) {
    let mut instr = line.split(' ');
    let dir = Dir::new(instr.next().unwrap());
    let n = instr.next().unwrap().parse::<usize>().unwrap();
    (dir, n)
}

/// Compute the coordinates of a point moving one unit in a direction.
fn apply_move(p: &Point, d: &Dir) -> Point {
    match d {
        Dir::Up => (p.0, p.1 + 1),
        Dir::Down => (p.0, p.1 - 1),
        Dir::Left => (p.0 - 1, p.1),
        Dir::Right => (p.0 + 1, p.1),
    }
}

/// Compute the coordinates of a point following another one.
fn follow(tl: &Point, hd: &Point) -> Point {
    // We use manhattan distance to decide on a behavior :
    match manhattan(hd, tl) {
        0 | 1 => *tl, // overlapping or neighboring points do not move
        2 if is_diag(hd, tl) => *tl, // Diagonally neighboring points do not move
        2 => {
            // Move once in a straight line
            match (tl.0 - hd.0, tl.1 - hd.1) {
                (-2, _) => apply_move(tl, &Dir::Right),
                (2, _) => apply_move(tl, &Dir::Left),
                (_, -2) => apply_move(tl, &Dir::Up),
                (_, 2) => apply_move(tl, &Dir::Down),
                _ => unreachable!(),
            }
        }
        3 | 4 => {
            // Move twice (once in each direction)
            let inter = match tl.0 - hd.0 {
                x if x < 0 => apply_move(tl, &Dir::Right),
                x if x > 0 => apply_move(tl, &Dir::Left),
                _ => unreachable!(),
            };
            match tl.1 - hd.1 {
                y if y < 0 => apply_move(&inter, &Dir::Up),
                y if y > 0 => apply_move(&inter, &Dir::Down),
                _ => unreachable!(),
            }
        }
        _ => unreachable!(),
    }
}

/// Manhattan distance between two points.
fn manhattan(p1: &Point, p2: &Point) -> usize {
    p1.0.abs_diff(p2.0) + p1.1.abs_diff(p2.1)
}

/// Are the two points on the same diagonal ?
fn is_diag(p1: &Point, p2: &Point) -> bool {
    let x = (p1.0 - p2.0).abs();
    let y = (p1.1 - p2.1).abs();
    x != 0 && x == y
}

#[cfg(test)]
mod tests {
    use crate::days::day09::*;

    #[test]
    fn test_day09_solve1() {
        let test_data = "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";
        assert_eq!(solve1(test_data), 13)
    }

    #[test]
    fn test_day09_solve2() {
        let test_data = "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";
        assert_eq!(solve2(test_data), 36)
    }
}
