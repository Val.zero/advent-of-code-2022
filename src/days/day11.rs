use std::ops::Div;
use regex::Regex;

pub fn solve1(input: &str) -> usize {
    let mut monkeys = input.lines().filter(|l|l != &"").map(|l| l.trim()).collect::<Vec<_>>().chunks(6).map(Monkey::new).collect::<Vec<_>>();
    let monkeys_num = monkeys.len();

    for _ in 0..20 {
        for n in 0..monkeys_num {
            let monkey_clone = monkeys.get(n).unwrap().clone();
            for item in monkey_clone.items {
                let new_val = monkey_clone.op_code.apply(item, monkey_clone.op_left, monkey_clone.op_right).div(3);
                if new_val % monkey_clone.test == 0 {
                    monkeys.get_mut(monkey_clone.then_throw).unwrap().items.push(new_val)
                } else {
                    monkeys.get_mut(monkey_clone.else_throw).unwrap().items.push(new_val)
                }
            }
            let monkey = monkeys.get_mut(n).unwrap();
            monkey.inspections += monkey.items.len();
            monkey.items = Vec::new();
        }
    }

    let mut inspections_values = monkeys.iter().map(|m| m.inspections).collect::<Vec<_>>();
    inspections_values.sort();
    inspections_values[inspections_values.len() - 1] * inspections_values[inspections_values.len() - 2]
}

pub fn solve2(input: &str) -> usize {
    let mut monkeys = input.lines().filter(|l|l != &"").map(|l| l.trim()).collect::<Vec<_>>().chunks(6).map(Monkey::new).collect::<Vec<_>>();
    let monkeys_num = monkeys.len();

    for _ in 0..10000 {
        for n in 0..monkeys_num {
            let monkey_clone = monkeys.get(n).unwrap().clone();
            for item in monkey_clone.items {
                let new_val = monkey_clone.op_code.wrapping_apply(item, monkey_clone.op_left, monkey_clone.op_right);
                if test(item, new_val, monkey_clone.test) {
                    monkeys.get_mut(monkey_clone.then_throw).unwrap().items.push(new_val)
                } else {
                    monkeys.get_mut(monkey_clone.else_throw).unwrap().items.push(new_val)
                }
            }
            let monkey = monkeys.get_mut(n).unwrap();
            monkey.inspections += monkey.items.len();
            monkey.items = Vec::new();
        }
    }

    let mut inspections_values = monkeys.iter().map(|m| m.inspections).collect::<Vec<_>>();
    inspections_values.sort();
    inspections_values[inspections_values.len() - 1] * inspections_values[inspections_values.len() - 2]
}

#[derive(Clone)]
struct Monkey {
    items: Vec<usize>,
    op_code: Op,
    op_left: Operand,
    op_right: Operand,
    test: usize,
    then_throw: usize,
    else_throw: usize,
    inspections: usize,
}

impl Monkey {
    fn new(info: &[&str]) -> Self {
        let mut lines = info.iter();
        lines.next().unwrap();

        let num_re = Regex::new(r"\d+").unwrap();
        let items = num_re.find_iter(lines.next().unwrap()).filter_map(|d|d.as_str().parse::<usize>().ok()).collect::<Vec<_>>();

        let op_re = Regex::new(r"Operation: new = (?P<left>\d+|old) (?P<op>[+*]) (?P<right>\d+|old)").unwrap();
        let op_match = op_re.captures(lines.next().unwrap()).unwrap();
        let op_code = Op::new(&op_match["op"]);
        let op_left = Operand::new(&op_match["left"]);
        let op_right = Operand::new(&op_match["right"]);

        let test = num_re.find(lines.next().unwrap()).unwrap().as_str().parse::<usize>().unwrap();
        let then_throw = num_re.find(lines.next().unwrap()).unwrap().as_str().parse::<usize>().unwrap();
        let else_throw = num_re.find(lines.next().unwrap()).unwrap().as_str().parse::<usize>().unwrap();

        Monkey{
            items,
            op_code,
            op_left,
            op_right,
            test,
            then_throw,
            else_throw,
            inspections: 0,
        }
    }
}

fn test(old_val: usize, new_val: usize, test_val: usize) -> bool {
    if old_val <= new_val {
        new_val % test_val == 0
    } else {
        (new_val + (usize::MAX % test_val)) % test_val == 0
    }
}

#[derive(Copy, Clone)]
enum Op {
    Add,
    Mul,
}

impl Op {
    fn new(s: &str) -> Self {
        match s {
            "+" => Op::Add,
            "*" => Op::Mul,
            _ => panic!("Wrong operator: {}", s)
        }
    }

    fn apply(&self, old: usize, left: Operand, right: Operand) -> usize {
        let l_val = match left {
            Operand::Old => old,
            Operand::Num(n) => n,
        };
        let r_val = match right {
            Operand::Old => old,
            Operand::Num(n) => n,
        };
        match self {
            Op::Add => l_val + r_val,
            Op::Mul => l_val * r_val,
        }
    }

    fn wrapping_apply(&self, old: usize, left: Operand, right: Operand) -> usize {
        let l_val = match left {
            Operand::Old => old,
            Operand::Num(n) => n,
        };
        let r_val = match right {
            Operand::Old => old,
            Operand::Num(n) => n,
        };
        match self {
            Op::Add => l_val.wrapping_add(r_val),
            Op::Mul => l_val.wrapping_mul(r_val),
        }
    }
}

#[derive(Copy, Clone)]
enum Operand {
    Old,
    Num(usize),
}

impl Operand {
    fn new(s: &str) -> Self {
        if s == "old" {
            Operand::Old
        } else if let Ok(n) = s.parse::<usize>() {
            Operand::Num(n)
        } else {
            panic!("Wrong operand: {}", s)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::days::day11::*;

    #[test]
    fn test_day11_solve1() {
        let test_data = "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";
        assert_eq!(solve1(test_data), 10605)
    }

    #[test]
    fn test_day11_solve2() {
        let test_data = "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";
        assert_eq!(solve2(test_data), 2713310158)
    }
}
