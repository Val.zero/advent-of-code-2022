use std::collections::HashMap;

pub fn solve1(input: &str) -> usize {
    mk_dir_struct(input)
        .values()
        .filter(|dir| dir <= &&100000)
        .sum()
}

pub fn solve2(input: &str) -> usize {
    let dirs = mk_dir_struct(input);
    // the amount of free space needed is 30000000 minus the current amount of space (70000000 minus
    // the size of the root directory)
    let free_space = dirs["/"] - 40000000;
    *dirs.values()
        .filter(|dir| dir >= &&free_space)
        .reduce(|acc, dir| if dir < acc { dir } else { acc })
        .unwrap()
}

/// Make the directory structure according to the cd instructions in a given input. Directories' size
/// is computed.
fn mk_dir_struct(input: &str) -> HashMap<String, usize> {
    // Initiate a map referencing every directory by its full path, starting by root
    let mut dirs = HashMap::new();
    let mut cur_dir = "/".to_string();
    dirs.insert(
        cur_dir.clone(),
        0,
    );

    // Reading lines except for ls commands which can be ignored
    for line in input.lines().filter(|l| l != &"$ ls") {
        let (hd, tl) = line.split_once(' ').unwrap();
        match hd {
            "$" => {
                // Commands can only be cd
                let (_, dir) = tl.split_once(' ').unwrap();
                match dir {
                    "/" => {
                        // We make the hypothesis that this is only encountered once at the beginning
                        // of the input
                        continue;
                    }
                    ".." => {
                        // Directories' size get updated as we move up a level
                        cur_dir = updir(&cur_dir, &mut dirs);
                    }
                    _ => {
                        // A new directory is encountered (we make the hypothesis that directories are
                        // visited only once) : add it to the global map.
                        cur_dir = cur_dir.clone() + dir + "/";
                        dirs.insert(
                            cur_dir.clone(),
                            0,
                        );
                    }
                }
            }
            "dir" => {
                // This is part of the result of an ls command, but useless to us
                continue;
            }
            // This is part of the result of an ls command : a file's size is added to the current directory's
            // size
            _ => {
                dirs.insert(cur_dir.clone(), dirs[&cur_dir] + hd.parse::<usize>().unwrap()).unwrap();
            }
        }
    }

    // When every command has been read, go back to root and update upper directories' size
    while cur_dir != *"/" {
        cur_dir = updir(&cur_dir, &mut dirs);
    }
    dirs
}

/// Get the path to the directory containing the given current one, and update its size by adding the
/// current one's.
fn updir(cur_dir: &str, dirs: &mut HashMap<String, usize>) -> String {
    let path = cur_dir
        .split('/')
        .filter(|elem| elem != &"")
        .collect::<Vec<_>>();
    let mut up_dir = "/".to_string();
    for item in &path[..path.len() - 1] {
        up_dir += &*(item.to_string() + "/")
    }
    dirs.insert(up_dir.clone(), dirs[&up_dir] + dirs[cur_dir]).unwrap();
    up_dir
}

#[cfg(test)]
mod tests {
    use crate::days::day07::*;

    #[test]
    fn test_day07_solve1() {
        let test_data = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";
        assert_eq!(solve1(test_data), 95437)
    }

    #[test]
    fn test_day07_solve2() {
        let test_data = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";
        assert_eq!(solve2(test_data), 24933642)
    }
}
