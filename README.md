# Advent of code 2022

My solutions for Advent of Code's 2022 edition's puzzles, written in Rust. 🦀❤️🎄

## Use

Use cargo to build and run. Useful arguments :
- `-d <days>` to specify which days to run, with `<days>` a number from `1` to `25`, a range (`3-5`), or a list of both
  (`"5, 1-3, 9"`)
- `-f` to run each day's first puzzle's solution
- `-s` to run each day's second puzzle's solution
- `-v` to enable verbose mode (not so verbose actually)
- `-t` to enable timing of the executions

## Progression

[████      ]

- Day 01 : ⭐⭐
- Day 02 : ⭐⭐
- Day 03 : ⭐⭐
- Day 04 : ⭐⭐
- Day 05 : ⭐⭐
- Day 06 : ⭐⭐
- Day 07 : ⭐⭐
- Day 08 : ⭐⭐
- Day 09 : ⭐⭐
- Day 10 : ⭐⭐
- Day 11 : ⭐

## Previous
- [2021 (Rust)](https://gitlab.com/Val.zero/advent-of-code-2021)
- [2020 (Rust)](https://gitlab.com/Val.zero/advent-of-code-2020)