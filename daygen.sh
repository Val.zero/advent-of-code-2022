#! /bin/bash
# Day generator
# Use : call with the two-digit day number you want to generate.

dayNum=$1
if [ "$dayNum" ]; then
  # Day file template
  printf "pub fn solve1(input: &str) -> usize {
    unimplemented!()
}

pub fn solve2(input: &str) -> usize {
    unimplemented!()
}

#[cfg(test)]
mod tests {
    use crate::days::day%s::*;

    #[test]
    fn test_day%s_solve1() {
        let test_data = \"\";
        assert_eq!(solve1(test_data), 0)
    }

    #[test]
    fn test_day%s_solve2() {
        let test_data = \"\";
        assert_eq!(solve2(test_data), 0)
    }
}
" "$dayNum" "$dayNum" "$dayNum" >> ./src/days/day"$dayNum".rs
  # Add to mod.rs
  printf "pub mod day%s;\n" "$dayNum" >> ./src/days/mod.rs
  # Add to util::solver
  sed -i "121 s/\[/[\n        ($dayNum, (day$dayNum::solve1 as for<'r> fn(\&'r str) -> usize, day$dayNum::solve2 as for<'r> fn(\&'r str) -> usize)),/" ./src/util.rs
  echo "Generated day $dayNum"
else
 echo "Please provide a day number"
fi
